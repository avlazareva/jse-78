package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.ITaskDtoService;
import ru.t1.lazareva.tm.exception.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.NameEmptyException;
import ru.t1.lazareva.tm.exception.UserIdEmptyException;
import ru.t1.lazareva.tm.model.TaskDTO;
import ru.t1.lazareva.tm.repository.TaskDtoRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Transactional
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDtoService implements ITaskDtoService {

    @Nullable
    @Autowired
    private TaskDtoRepository repository;

    @SneakyThrows
    @Override
    @Transactional
    public void save(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new EntityNotFoundException();
        task.setUserId(userId);
        repository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null) throw new EntityNotFoundException();
        for (@NotNull TaskDTO taskDto : tasks) {
            if (taskDto.getName() == null || taskDto.getName().isEmpty()) throw new NameEmptyException();
            taskDto.setUserId(userId);
        }
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

}