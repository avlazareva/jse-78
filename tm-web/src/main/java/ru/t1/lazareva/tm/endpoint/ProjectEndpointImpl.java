package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.IProjectDtoService;
import ru.t1.lazareva.tm.model.CustomUser;
import ru.t1.lazareva.tm.model.ProjectDTO;

@RestController
@RequestMapping("/api/project")
public class ProjectEndpointImpl {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @GetMapping("/{id}")
    public ProjectDTO get(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        return projectService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody ProjectDTO project) {
        projectService.save(user.getUserId(), project);
    }

    @PutMapping
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody ProjectDTO project) {
        projectService.save(user.getUserId(), project);
    }

    @DeleteMapping("/{id}")
    public void delete(@AuthenticationPrincipal final CustomUser user, @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(user.getUserId(), id);
    }

}