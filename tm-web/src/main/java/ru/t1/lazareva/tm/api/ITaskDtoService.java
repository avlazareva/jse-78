package ru.t1.lazareva.tm.api;

import ru.t1.lazareva.tm.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService {

    void save(final String userId, final TaskDTO task);

    void saveAll(final String userId, final Collection<TaskDTO> tasks);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<TaskDTO> findAll(final String userId);

    TaskDTO findOneById(final String userId, final String id);

}