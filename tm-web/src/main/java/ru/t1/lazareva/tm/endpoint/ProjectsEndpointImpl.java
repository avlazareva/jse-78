package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.IProjectDtoService;
import ru.t1.lazareva.tm.model.CustomUser;
import ru.t1.lazareva.tm.model.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsEndpointImpl {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @GetMapping
    public List<ProjectDTO> get(@AuthenticationPrincipal final CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

    @PostMapping
    public void post(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(user.getUserId(), projects);
    }

    @PutMapping
    public void put(@AuthenticationPrincipal final CustomUser user, @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(user.getUserId(), projects);
    }

    @DeleteMapping
    public void delete(@AuthenticationPrincipal final CustomUser user) {
        projectService.removeAll(user.getUserId());
    }

}